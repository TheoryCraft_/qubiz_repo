﻿var pressedList = []; // Array of used keys
var lineList = [];    // Array of points to place lines
var failCount = 7;
var divPosition = $('#drawing').position();
var errTexts = [
    'Oh come on...', 'Really? THAT letter?', "You're not that good at this, are you?", '9/11 would try that again', 'GG...'
];

$(document).ready(function () {
    createLineList();
    getWord();
    $(document).bind('keypress', function (e) {
        var char = String.fromCharCode(e.charCode).toUpperCase();
        handleInput(char);
    });

    $('.letter-input').click(function () {
        var $this = $(this);
        handleInput($this.text());
    });

    // Old deletion code
    /*$(".backspace-input").click(function () {
        var $main = $('#main-string');
        backspaceInput($main);
    });

    $('.clear-input').click(function () {
        $('#main-string').text(" ");
    });*/
});

function handleInput(charInput) {
    if (charInput.charCodeAt(0) < 65 || charInput.charCodeAt(0) > 90) return; // Input validation
    if (pressedList.indexOf(charInput) === -1) {
        var keysFound = 0;
        $('.hangman-covered').each(function () {
            if ($(this).text().toUpperCase() === charInput) {
                keysFound++;
                $(this).toggleClass('hangman-covered');
            }
        });
        mark_used(charInput);
        pressedList.push(charInput);
        if (keysFound === 0) renderFail();
        else updateWordData(keysFound);
        if (isWon()) renderWin();
        if (failCount === 0) renderLoss();
    } else {
        displayError('This key has already been pressed!', 1000);
    }
}

// Marks used letters in a red font color + a dash in the DOM
function mark_used(charInput) {
    $('.letter-input').each(function () {
        if ($(this).text() === charInput) {
            $(this).toggleClass('letter-input-used');
        }
    });
}

// Visual rep of failCount (probs stickman) and failCount updating
function renderFail() {
    var errIndex = getRandomInt(0, errTexts.length);
    displayError(errTexts[errIndex], 1000);
    drawShape(7 - failCount);
    failCount--;
}

// Loss message + option to restart WIP
function renderLoss() {
    $('.hangman-covered').each(function () {
        $(this).toggleClass('hangman-covered');
        $(this).toggleClass('hangman-reveal');
    });
    var res = confirm('Restart?');
    if (res) restart();
}

// Custom win graphics WIP
function renderWin() {
    var res = confirm('WON! Restart?');
    if (res) restart();
}

function updateWordData(keysFound) {
    var scope = angular.element($('#main-screen')).scope();
    scope.$apply(function () {
        scope.letterCount -= keysFound;
    });
}

function isWon() {
    var scope = angular.element($('#main-screen')).scope();
    if (scope.letterCount !== 0) return false;
    return true;
}

function restart() {
    clearShapes();
    $('.hangman').each(function () {
        $(this).addClass('hangman-covered');
        if ($(this).hasClass('hangman-reveal'))
            $(this).removeClass('hangman-reveal');
    });

    // Allows the game to clear propperly, so you can't see the word after it was generated
    setTimeout(function () {
        failCount = 7;
        pressedList = [];
        getWord();
        $('.letter-input').each(function () {
            if ($(this).hasClass('letter-input-used')) $(this).removeClass('letter-input-used');
        });
    }, 100);
}

// Sets the collapse to display a custom text for a specified time
function displayError(errText, delay) {
    setErrorText(errText);
    $('#error-alert').collapse("show");
    setTimeout(function () { $('#error-alert').collapse("hide") }, delay);
}

function setErrorText(erText) {
    var scope = angular.element($('#main-screen')).scope();
    scope.$apply(function () {
        scope.errText = erText;
    });
}

function drawShape(shapeIndex) {
    var shapeClass = 'noClass';
    switch (shapeIndex) {
        case 0: {
            for (var i = 0; i < 4; i++) { drawAfterDelay(i, 'line', i * 500); }
            break;
        }
        case 1: {
            createCircle(lineList[3].x2 - 2, lineList[3].y2 - 2, 'circle');
            break;
        }
        case 2:
        case 3:
        case 4:
        case 5:
        case 6: {
            shapeIndex += 2;
            drawLine(shapeIndex, 'line');
            break;
        }
    }
}

function drawLine(lineIndex, lineClass) {
    createLine(lineList[lineIndex].x1, lineList[lineIndex].y1, lineList[lineIndex].x2, lineList[lineIndex].y2, lineClass);
}

function createLine(x1, y1, x2, y2, lineClass) {
    var length = Math.sqrt((x1 - x2) * (x1 - x2) + (y1 - y2) * (y1 - y2));
    var angle = Math.atan2(y2 - y1, x2 - x1) * 180 / Math.PI;
    var transform = 'rotate(' + angle + 'deg)';
    var line = $('<div>')
            .appendTo('#drawing')
            .addClass(lineClass)
            .css({
                'position': 'absolute',
                '-webkit-transform': transform,
                '-moz-transform': transform,
                'transform': transform
            })
            .width(0)
            .offset({ left: x1, top: y1 });
    setTimeout(function () { line.width(length); }, 450);
    return line;
}

function createCircle(x, y, circleClass) {
    var circle = $('<div>')
                .appendTo('#drawing')
                .addClass(circleClass)
                .css({
                    'position': 'absolute'
                })
                .offset({ left: x - 25, top: y })
                .width(0)
                .height(0);
    setTimeout(function () { circle.width(50); circle.height(50); }, 5);
    return circle;
}

function createLineList() {
    lineList[0] = { x1: divPosition.left + 450, y1: divPosition.top + 320, x2: divPosition.left + 650, y2: divPosition.top + 320 };
    lineList[1] = { x1: getMiddleX(lineList[0]) - 20, y1: lineList[0].y1, x2: getMiddleX(lineList[0]) - 20, y2: lineList[0].y1 - 250 };
    lineList[2] = { x1: lineList[1].x2, y1: lineList[1].y2, x2: lineList[1].x2 + 100, y2: lineList[1].y2 };
    lineList[3] = { x1: lineList[2].x2, y1: lineList[2].y2, x2: lineList[2].x2, y2: lineList[2].y2 + 30 };
    lineList[4] = { x1: lineList[3].x2, y1: lineList[3].y2 + 53, x2: lineList[3].x2, y2: lineList[3].y2 + 125 };
    lineList[5] = { x1: lineList[4].x2, y1: lineList[4].y1 + 10, x2: lineList[3].x2 - 25, y2: lineList[4].y1 + 15 };
    lineList[6] = { x1: lineList[5].x1, y1: lineList[5].y1, x2: lineList[3].x2 + 25, y2: lineList[5].y2 };
    lineList[7] = { x1: lineList[4].x2, y1: lineList[4].y2 - 5, x2: lineList[4].x2 - 30, y2: lineList[4].y2 + 20 };
    lineList[8] = { x1: lineList[7].x1, y1: lineList[7].y1, x2: lineList[4].x2 + 30, y2: lineList[7].y2 };
}

function getMiddleX(line) {
    return (line.x1 + line.x2) / 2;
}

function drawAfterDelay(lineIndex, lineClass, delay) {
    setTimeout(function () { drawLine(lineIndex, lineClass); }, delay);
}

function clearShapes() {
    $('.line').each(function () {
        $(this).remove();
    });
    $('.circle').remove();
}

function getWord() {
    var scope = angular.element($('#main-screen')).scope();
    var word = scope.wordString.join('');
    var index = getRandomInt(0, scope.wordList.length);
    while (scope.wordList[index] === word)
        index = getRandomInt(0, scope.wordList.length);
    var word = scope.wordList[index];
    scope.$apply(function () {
        scope.wordString = [];
        for (var i = 0; i < word.length; i++) {
            scope.wordString.push(word[i]);
        }
        scope.letterCount = word.length;
    });
}

function getRandomInt(min, max) {
    return Math.floor(Math.random() * (max - min)) + min;
}

// Old event handler not used anymore
/*
 function backspaceInput($main) {
    var text = $main.text();
    if (text == 'Typed string will be here!' || text == " ") {
        $main.text(" ");
    } else {
        $main.text(text.substr(0, text.length - 1));
    }
}
*/