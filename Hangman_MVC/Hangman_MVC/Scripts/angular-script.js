﻿var mainApp = angular.module('hangman', []);

// ! -- UNUSED -- !
mainApp.factory('GameService', function () {
    var factory = {};
    factory.firstPress = function (pressedList, charInput) {
        var result = pressedList.indexOf(charInput);
        if (result === -1) return true;
        return false;
    };
    factory.addPress = function (pressedList, charInput) {
        return pressedList.concat(charInput);
    };
    return factory;
});

//Initialising the game constants and variables
mainApp.controller('GameController', ['$scope', function ($scope) {
        $scope.wordList = [ 'Bobocel', 'Dinamita', 'Cosmonaut', 'Arhitect', 'Barza'
        ];
        $scope.errText = "you shouldn't see this";
        $scope.isCollapsed = false;
        $scope.letters = [];
        $scope.wordString = [];
        $scope.letterCount = 0;
        for (var i = 65; i <= 90; i++) {
            $scope.letters.push(String.fromCharCode(i));
        }

        // UNUSED
        $scope.getTemplate = function(charInput) {
            if (charInput.charCodeAt(0) >= 65 && charInput.charCodeAt(0) <= 90) return 'letterKey';
            return 'emptyKey';
        };
    }]);


