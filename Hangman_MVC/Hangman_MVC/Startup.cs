﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(Hangman_MVC.Startup))]
namespace Hangman_MVC
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
