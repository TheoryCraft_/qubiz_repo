﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Homework_DLL
{
    public class Building : IGeographicalObject
    {
        //IGeographicalObject implementation
        public int N { get; set; }
        public int S { get; set; }
        public int E { get; set; }
        public int W { get; set; }
        public int Altitude { get; set; } 

        //Properties
        public string ID { get; set; }
        public string Location { get; set; }
        public int Floors { get; set; }
        public int Height { get; set; }
        public int Width { get; set; }
        public int Length { get; set; }
        public List<Apartment> ApartmentList { get; set; }
        
        //Constructors
        public Building() { ApartmentList = new List<Apartment>(); this.Location = null; this.Width = 0; this.Height = 0; this.Length = 0; this.Floors = 0; }
        public Building(string ID ,int N ,int S ,int E ,int W ,int Altitude ,int Width ,int Height ,int Length ,int Floors)
        {
            this.ID = ID;
            this.N = N;
            this.E = E;
            this.W = W;
            this.S = S;
            this.Altitude = Altitude;
            ApartmentList = new List<Apartment>();
            this.Location = null;
            this.Width = Width;
            this.Height = Height;
            this.Length = Length;
            this.Floors = Floors;
        }
    }
}
