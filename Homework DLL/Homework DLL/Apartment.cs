﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Homework_DLL
{
    public class Apartment : IOwnableObject , IGeographicalObject
    {
        //IGeographicalObject implementation
        public int N { get; set; }
        public int S { get; set; }
        public int E { get; set; }
        public int W { get; set; }
        public int Altitude { get; set; }

        //IOwnableObject implementation
        public string Owner { get; set; }
        public string Name { get; set; }
        public decimal Price { get; set; }
        public decimal Taxing { get; set; }

        //Properties
        public int Floor { get; set; }
        public string Street { get; set; }
        public int Width { get; set; }
        public int Length { get; set; }
        public int Height { get; set; }
        public List<IOwnableObject> ApartmentObjects { get; set; }

        //Constructors
        public Apartment() { Owner = "NONE"; Name = null; Price = 0; Taxing = 0; Width = 0; Length = 0; Height = 0; ApartmentObjects = new List<IOwnableObject>(); }
        public Apartment(int N ,int S ,int E ,int W ,int Altitude ,int Floor ,string Street ,string Name,decimal Price,decimal Taxing,int Width,int Height,int Length)
        {
            this.N = N;
            this.S = S;
            this.E = E;
            this.W = W;
            this.Floor = Floor;
            this.Street = Street;
            this.Altitude = Altitude;
            this.Owner = "NONE";
            this.Name = Name;
            this.Price = Price;
            this.Taxing = Taxing;
            this.Width = Width;
            this.Height = Height;
            this.Length = Length;
            ApartmentObjects = new List<IOwnableObject>();
        }
    }
}
