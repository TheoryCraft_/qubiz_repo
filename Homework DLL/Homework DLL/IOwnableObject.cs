﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Homework_DLL
{
    public interface IOwnableObject
    {
        string Owner { get; set; }
        string Name { get; set; }
        decimal Price { get; set; }
        decimal Taxing { get; set; }
    }
}
