﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Homework_DLL
{
    public class City : IGeographicalObject
    {
        //IGeographicalObject implementation
        public int N { get; set; }
        public int S { get; set; }
        public int E { get; set; }
        public int W { get; set; }
        public int Altitude { get; set; }

        //Properties
        public string Name { get; set; }
        public Human Mayor { get; set; }
        public List<Street> Streets { get; set; }
        public List<Intersection_Node> Intersections { get; set; }
        public List<IGeographicalObject> PlacesOfInterest { get; set; }
        public List<Building> Institutions { get; set; }

        //Constructors
        public City()
        {
            Name = "NONAME";
            Mayor = null;
            Streets = new List<Street>();
            Intersections = new List<Intersection_Node>();
            PlacesOfInterest = new List<IGeographicalObject>();
            Institutions = new List<Building>();
        }
        public City(string Name,Human Mayor)
        {
            this.Name = Name;
            this.Mayor = Mayor;
            Streets = new List<Street>();
            Intersections = new List<Intersection_Node>();
            PlacesOfInterest = new List<IGeographicalObject>();
            Institutions = new List<Building>();
        }
    }
}
