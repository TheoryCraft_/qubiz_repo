﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Homework_DLL
{
    public class Human 
    {
        //Properties
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string SSN { get; set; }
        public string Address { get; set; }
        public string Race { get; set; }
        public List<IOwnableObject> OwnershipList { get; set; }
        public decimal IncomeMonthly { get; set; }
        public decimal Taxes { get; set; }

        //Constructors
        public Human() { this.FirstName = null; this.LastName = null; this.SSN = null; this.Address = null; this.OwnershipList = null; }
        public Human(string FirstName ,string LastName, string SSN, string Address, List<IOwnableObject> OwnershipList ,decimal IncomeMonthly)
        {
            this.FirstName = FirstName;
            this.LastName = LastName;
            this.SSN = SSN;
            this.Address = Address;
            this.OwnershipList = OwnershipList;
            this.IncomeMonthly = IncomeMonthly;
        }

        //Helper methods
        private decimal calculateTaxes()
        {
            decimal s = 0;
            foreach (IOwnableObject obj in OwnershipList) s += obj.Taxing;
            return s;
        }
        public decimal getNetWorth() { return IncomeMonthly - Taxes; }
    }
}
