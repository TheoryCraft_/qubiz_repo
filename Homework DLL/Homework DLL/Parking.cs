﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Homework_DLL
{
    public class Parking : IGeographicalObject
    {
        // IGeographicalObject implementation
        public int N { get; set; }
        public int S { get; set; }
        public int E { get; set; }
        public int W { get; set; }
        public int Altitude { get; set; }

        public Car ParkedCar { get; set; }
        public decimal ParkingTax { get; set; }

        //Constructors
        public Parking () { N = 0; S = 0; W = 0; E = 0; Altitude = 0; ParkedCar = null; ParkingTax = 0; }
        public Parking(int N, int S, int E, int W, int Altitude, Car ParkedCar, int ParkingTax)
        {
            this.N = N;
            this.S = S;
            this.E = E;
            this.W = W;
            this.Altitude = Altitude;
            this.ParkedCar = ParkedCar;
            this.ParkingTax = ParkingTax;
        }
    }
}
