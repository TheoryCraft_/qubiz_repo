﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Homework_DLL
{
    public class List_Manipulation_Methods
    {
        public static void addElement<T>(ref List<T> objectList, List<T> addList) { objectList = addList; }
        public static bool addElement<T>(ref List<T> objectList, T item)
        {
            foreach (T obj in objectList)
                if (obj.Equals(item))
                    return false;
            objectList.Add(item);
            return true;
        }
        public static bool removeElement<T>(ref List<T> objectList , T item) { return objectList.Remove(item); }
    }
}
