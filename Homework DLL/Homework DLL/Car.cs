﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Homework_DLL
{
    public class Car : IOwnableObject
    {
        //IOwnableObject implementation
        public string Owner { get; set; }
        public string Name { get; set; }
        public decimal Price { get; set; }
        public decimal Taxing { get; set; }

        //Properties
        public string Manufacturer { get; set; }
        public string Model { get; set; }
        public struct CarEngine {
            string Type;
            string Capacity;
            string Horsepower;
            public CarEngine(string Type ,string Capacity ,string Horsepower)
            {
                this.Type = Type;
                this.Capacity = Capacity;
                this.Horsepower = Horsepower;    
            }
        };
        public CarEngine Engine { get; set; }
        public string WheelType { get; set; }
        public string Color { get; set; }
        string LicencePlate { get; set; }

        //Constructors
        public Car() { Manufacturer = null; Model = null;  Engine = new CarEngine(); WheelType = null; Color = null; LicencePlate = null; Name = null; Price = 0; Taxing = 0; }
        public Car(string Owner, string Manufacturer ,string Model ,string EngineType ,string EngineCapacity ,string EngineHorsepower ,string WheelType ,string Color ,string LicencePlate ,decimal Price ,decimal Taxing)
        {
            this.Owner = Owner;
            this.Manufacturer = Manufacturer;
            this.Model = Model;
            this.Name = Manufacturer + " " + Model;
            this.Engine = new CarEngine(EngineType, EngineCapacity, EngineHorsepower);
            this.WheelType = WheelType;
            this.Color = Color;
            this.LicencePlate = LicencePlate;
            this.Price = Price;
            this.Taxing = Taxing;
        }
    }
}
