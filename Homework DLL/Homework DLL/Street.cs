﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Homework_DLL
{
    public class Street
    {
        public string Name { get; set; }
        public List<Building> BuildingList { get; set; }
        public List<Parking> ParkingList { get; set; }
        public List<IOwnableObject> MiscObjects { get; set; }
        public List<Car> PassingTraffic { get; set; }

        //Constructors
        public Street ()
        {
            Name = "NONAME";
            BuildingList = new List<Building>();
            ParkingList = new List<Parking>();
            MiscObjects = new List<IOwnableObject>();
            PassingTraffic = new List<Car>();
        }
        public Street(string Name)
        {
            this.Name = Name;
            BuildingList = new List<Building>();
            ParkingList = new List<Parking>();
            MiscObjects = new List<IOwnableObject>();
            PassingTraffic = new List<Car>();
        }
        
        //Helper methods
        public bool trafficToParking(Parking p, Car c)
        {
            if (p.ParkedCar != null) return false;
            List<Car> tempList = PassingTraffic;
            if (!List_Manipulation_Methods.removeElement<Car>(ref tempList, c)) return false;
            tempList = PassingTraffic;
            p.ParkedCar = c;
            return true;
        }
        public bool parkingToTraffic(Parking p, Car c)
        {
            if (p.ParkedCar == null) return false;
            List<Car> tempList = PassingTraffic;
            if (!List_Manipulation_Methods.addElement<Car>(ref tempList, c)) return false;
            PassingTraffic = tempList;
            p.ParkedCar = null;
            return true;
        }
    }
}
