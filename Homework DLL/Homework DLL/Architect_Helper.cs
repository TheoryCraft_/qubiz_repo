﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Homework_DLL
{
    public class Architect_Helper
    {
        public static int getArea(int Width,int Length) { return Width * Length; }
        public static int getVolume(int Width,int Height,int Length) { return getArea(Width, Length) * Height; }
        public static int getFloorHeight(int Height, int Floors) { return Height / Floors; }
    }
}
