﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Homework_DLL
{
    public interface IGeographicalObject
    {
        int N { get; set; }
        int S { get; set; }
        int E { get; set; }
        int W { get; set; }
        int Altitude { get; set; }
    }
}
