﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Homework_DLL
{
    public class Intersection_Node : IGeographicalObject 
    {
        //IGeographicalObject implementation
        public int N { get; set; }
        public int S { get; set; }
        public int E { get; set; }
        public int W { get; set; }
        public int Altitude { get; set; }

        //Properties
        public string Name { get; set; }
        public List<Intersection_Node> ConnectionList { get; set; } 

        //Constructor
        public Intersection_Node() { Name = null; ConnectionList = new List<Intersection_Node>(); N = 0; S = 0; E = 0; W = 0; Altitude = 0; }
        public Intersection_Node(int N,int S,int W,int E,int Altitude,string Name)
        {
            this.N = N;
            this.S = S;
            this.W = W;
            this.E = E;
            this.Altitude = Altitude;
            this.Name = Name;
            this.ConnectionList = new List<Intersection_Node>();
        }
    }
}
