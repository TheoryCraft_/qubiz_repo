﻿using System;
using Homework_DLL;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Homework_DLL.Test
{
    [TestClass]
    public class Human_Test
    {
        [TestMethod]
        public void TestMethod1()
        {
            Human m = new Human();
            m.FirstName = "Popescu";
            m.LastName = "Ion";
            m.Race = "Caucasian";
            m.SSN = "1990533055048";
            m.Taxes = 0; //Duh
            m.IncomeMonthly = 999999; //Duh
            m.Address = "Primaria"; //Duh
            City c = new City("SmallVille", m);
            Console.WriteLine(c.Mayor.ToString());
            Console.ReadKey();
        }
    }
}
